"""linguine: a command line tool for tokenizing text."""
from argparse import ArgumentParser
from json import dumps
from sys import stdin, stdout
from typing import TYPE_CHECKING

from spacy import load

if TYPE_CHECKING:
    from argparse import Namespace
    from types import TracebackType
    from typing import (
        Any,
        Callable,
        Dict,
        List,
        Literal,
        Optional,
        Set,
        TextIO,
        Type,
    )

    from spacy.language import DisabledPipes, Language
    from spacy.tokens import Doc, Token


TOKEN_ATTRIBUTES: "Dict[str, Callable[[Token], Any]]" = {
    "dep_": lambda t: t.dep_,
    "ent_id_": lambda t: t.ent_id_,
    "ent_iob_": lambda t: t.ent_iob_,
    "ent_kb_id_": lambda t: t.ent_kb_id_,
    "ent_type_": lambda t: t.ent_type_,
    "is_alpha": lambda t: t.is_alpha,
    "is_ascii": lambda t: t.is_ascii,
    "is_bracket": lambda t: t.is_bracket,
    "is_currency": lambda t: t.is_currency,
    "is_digit": lambda t: t.is_digit,
    "is_left_punct": lambda t: t.is_left_punct,
    "is_lower": lambda t: t.is_lower,
    "is_oov": lambda t: t.is_oov,
    "is_punct": lambda t: t.is_punct,
    "is_quote": lambda t: t.is_quote,
    "is_right_punct": lambda t: t.is_right_punct,
    "is_space": lambda t: t.is_space,
    "is_stop": lambda t: t.is_stop,
    "is_title": lambda t: t.is_title,
    "is_upper": lambda t: t.is_upper,
    "lang_": lambda t: t.lang_,
    "lemma_": lambda t: t.lemma_,
    "like_email": lambda t: t.like_email,
    "like_num": lambda t: t.like_num,
    "like_url": lambda t: t.like_url,
    "lower_": lambda t: t.lower_,
    "norm_": lambda t: t.norm_,
    "pos_": lambda t: t.pos_,
    "prefix_": lambda t: t.prefix_,
    "shape_": lambda t: t.shape_,
    "suffix_": lambda t: t.suffix_,
    "tag_": lambda t: t.tag_,
    "text": lambda t: t.text,
    "text_with_ws": lambda t: t.text_with_ws,
    "whitespace_": lambda t: t.whitespace_,
}


AVAILABLE_COMPONENTS: "List[str]" = [
    "tok2vec",
    "tagger",
    "parser",
    "senter",
    "ner",
    "attribute_ruler",
    "lemmatizer",
]


def parse_args() -> "Namespace":
    """Get the command line arguments from the user and return them in an ``argparse.Namespace``.

    Returns:
        An ``argparse.Namespace`` containing the command line arguments supplied by the user.
    """
    parser = ArgumentParser()

    parser.add_argument(
        "--model",
        default="en_core_web_sm",
        dest="model_name",
        help="spaCy language model name",
        type=str,
    )
    parser.add_argument(
        "--with-component",
        action="append",
        choices=AVAILABLE_COMPONENTS,
        default=[],
        dest="enabled_components",
        help="enable an additional spaCy pipeline component",
        type=str,
    )
    parser.add_argument(
        "--emit-attribute",
        action="append",
        choices=list(TOKEN_ATTRIBUTES.keys()),
        default=[],
        dest="selected_attribute_names",
        help="emit an additional spaCy Token attribute for each Token",
        type=str,
    )
    return parser.parse_args()


def get_token_data(
    doc: "Doc", selected_token_attributes: "Set[str]"
) -> "List[Dict[str, Any]]":
    """Create a dictionary for each ``spacy.tokens.Token`` within the given ``spacy.tokens.Doc``,
       where each dictionary contains the selected ``spacy.token.Token`` attributes. Returns a
       list of these dictionaries that respects their order within the ``spacy.tokens.Doc``.

    Args:
        doc: The ``spacy.tokens.Doc`` containing the ``spacy.tokens.Token`` objects.
        selected_token_attributes: Names of ``spacy.token.Token`` attributes whose values should be
                                   extracted for each ``spacy.token.Token`` within the
                                   ``spacy.tokens.Doc``.
    Returns:
        A list of dictionaries, where each dictionary contains the attributes requested in
        ``selected_token_attributes`` for the ``spacy.token.Token`` at the corresponding index
        in ``doc``.
    """
    return [
        {key: TOKEN_ATTRIBUTES[key](token) for key in selected_token_attributes}
        for token in doc
    ]


def read_input_str(input_io: "TextIO") -> "str":
    """Read all lines from ``input_io`` into a single string.

    Args:
        input_io: The file-like object from which to read the input string.
    Returns:
        The string contents of ``input_io``.
    """
    return input_io.read()


def write_output_str(message: "str", output_io: "TextIO") -> "None":
    """Write the given ``str`` to the file-like object ``output_io`` with a newline appended.

    Args:
        message: The ``str`` to write.
        output_io: The file-like object on which to write ``message``.
    """
    output_io.write(f"{message}\n")
    output_io.flush()


class Model:
    """Context manager that manages the state of the natural language processing pipeline.

    Args:
        model_name: The name of the ``spacy`` language model. The language model must be installed
                    separately from ``spacy``. See the ``Pipfile`` for an example of how
                    ``en_core_web_sm`` is installed.
    Keyword Args:
        enabled_components: A set of ``spacy`` pipeline components to enable when processing text.
    """

    def __init__(
        self, model_name: "str", enabled_components: "Optional[Set[str]]" = None
    ) -> "None":
        self.enabled_components: "Optional[Set[str]]" = enabled_components
        self.disabled: "Optional[DisabledPipes]" = None
        self.model: "Language" = load(model_name)

    def __enter__(self) -> "Model":
        if self.enabled_components is not None:
            self.disabled = self.model.select_pipes(enable=self.enabled_components)
        return self

    def __exit__(
        self,
        exc_type: "Optional[Type[BaseException]]",
        exc_value: "Optional[BaseException]",
        traceback: "Optional[TracebackType]",
    ) -> "Literal[False]":
        if self.disabled is not None:
            self.disabled.restore()
            self.disabled = None
        return False

    def get_token_data_as_json(
        self,
        input_text: "str",
        selected_token_attributes: "Set[str]",
    ) -> "List[Dict[str, Any]]":
        """For the text ``input_text``, create a list of dictionaries. Each of these
           dictionaries corresponds to a ``spacy.tokens.Token`` in the text, and contains
           the values of the attributes requested in ``selected_token_attributes``.

        Args:
            input_text: The string to analyze.
            selected_token_attributes: Names of ``spacy.token.Token`` attributes whose values should
                                       be extracted for each ``spacy.token.Token`` within
                                       ``input_text``.
        Returns:
            A list of dictionaries, where each dictionary contains data about the
            ``spacy.tokens.Token`` at the corresponding location within ``input_text``.
        """
        return get_token_data(self.model(input_text), selected_token_attributes)

    def get_token_data_as_str(
        self,
        input_text: "str",
        selected_token_attributes: "Set[str]",
    ) -> "str":
        """For the text ``input_text``, create a JSON string containing a list of dictionaries.
           Each of these dictionaries corresponds to a ``spacy.tokens.Token`` in the text, and
           contains the values of the attributes requested in ``selected_token_attributes``.

        Args:
            input_text: The string to analyze.
            selected_token_attributes: Names of ``spacy.token.Token`` attributes whose values should
                                       be extracted for each ``spacy.token.Token`` within
                                       ``input_text``.
        Returns:
            A JSON string containing a list of dictionaries, where each dictionary contains data
            about the ``spacy.tokens.Token`` at the corresponding location within ``input_text``.
        """
        return dumps(self.get_token_data_as_json(input_text, selected_token_attributes))


def run(
    model_name: "str",
    enabled_components: "Set[str]",
    selected_token_attributes: "Set[str]",
    input_io: "TextIO",
    output_io: "TextIO",
) -> "None":
    """Get text from ``input_io``. Run whatever text is received through a natural language
       processing pipeline, and write a JSON string to ``output_io`` containing data about the
       text's individual tokens.

    Args:
        model_name: The name of the ``spacy`` language model. The language model must be installed
                    separately from ``spacy``. See the ``Pipfile`` for an example of how
                    ``en_core_web_sm`` is installed.
        enabled_components: A set of ``spacy`` pipeline components to enable when processing text.
        selected_token_attributes: Names of ``spacy.token.Token`` attributes whose values should be
                                   extracted for each ``spacy.token.Token`` within the
                                   ``spacy.tokens.Doc``.
        input_io: A file-like object from which to read the text to analyze.
        output_io: A file-like object to which to write the natural language processing result in
                   the form of a JSON string.
    """
    with Model(model_name, enabled_components) as model:
        write_output_str(
            model.get_token_data_as_str(
                read_input_str(input_io), selected_token_attributes
            ),
            output_io,
        )


def main() -> "None":
    """Call the `run()` function with arguments extracted from command line options."""
    argv = parse_args()

    run(
        argv.model_name,
        set(argv.enabled_components),
        set(argv.selected_attribute_names),
        stdin,
        stdout,
    )


if __name__ == "__main__":
    main()
