from linguine import Model


def test_get_token_data_as_json_pos_one_word():
    with Model("en_core_web_sm") as model:
        word, *_ = model.get_token_data_as_json(
            "watch",
            set(["pos_"]),
        )
        assert word["pos_"] == "VERB"


def test_get_token_data_as_json_pos_basic_sentence():
    with Model("en_core_web_sm") as model:
        left, right, *_ = model.get_token_data_as_json(
            "a watch",
            set(["pos_"]),
        )
        assert left["pos_"] == "DET"
        assert right["pos_"] == "NOUN"


def test_get_token_data_as_json_pos_across_newline():
    with Model("en_core_web_sm") as model:
        left, space, right, *_ = model.get_token_data_as_json(
            "a\nwatch",
            set(["pos_"]),
        )
        assert left["pos_"] == "DET"
        assert space["pos_"] == "SPACE"
        assert right["pos_"] == "NOUN"
